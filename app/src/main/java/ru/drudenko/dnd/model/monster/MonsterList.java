package ru.drudenko.dnd.model.monster;

import java.util.List;

public class MonsterList {
    private List<Monster> dataList;


    public List<Monster> getDataList() {
        return dataList;
    }

    public void setDataList(List<Monster> dataList) {
        this.dataList = dataList;
    }
}
